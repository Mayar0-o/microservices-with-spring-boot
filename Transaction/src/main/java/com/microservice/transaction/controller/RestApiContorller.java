package com.microservice.transaction.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.microservice.transaction.Service.TransactionService;

import com.microservice.transaction.entities.Transaction;


@RestController
@RequestMapping("/transaction")
public class RestApiContorller {

	@Autowired
	private TransactionService transactionService;

	private static final Logger LOG = Logger.getLogger(RestApiContorller.class.getName());
	
	@PostMapping(value = "/{itemId}", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Transaction> Create(@RequestBody Transaction transaction,
			@PathVariable("itemId") Integer itemId) {

		return new ResponseEntity<Transaction>(transactionService.Add(transaction, itemId), HttpStatus.OK);
	}
	
	@RequestMapping( method = RequestMethod.GET)
	public String Get() {
		return "dsfsd";
	}


	@RequestMapping(value = "/get", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public List<Transaction> GetItem() {
		LOG.info("get ............");
		return transactionService.GetAll();
	}

}
