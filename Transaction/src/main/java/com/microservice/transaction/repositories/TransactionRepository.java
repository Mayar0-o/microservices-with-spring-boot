package com.microservice.transaction.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.microservice.transaction.entities.Transaction;

public interface  TransactionRepository extends JpaRepository<Transaction, Integer>{

}
