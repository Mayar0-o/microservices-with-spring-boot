package com.microservice.transaction.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.microservice.transaction.entities.Item;

public interface ItemRepository extends JpaRepository<Item, Integer> {

}
