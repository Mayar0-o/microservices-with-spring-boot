package com.microservice.transaction.Service;

import java.util.List;

import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.microservice.transaction.entities.Item;
import com.microservice.transaction.entities.Transaction;
import com.microservice.transaction.repositories.ItemRepository;
import com.microservice.transaction.repositories.TransactionRepository;

@Service
public class TransactionService {

	@Autowired
	private TransactionRepository transactionRepository;

	@Autowired
	private ItemRepository itemRepository;

	@Transactional(value = TxType.REQUIRES_NEW)
	public Transaction Add(Transaction _transaction, Integer itemId) {

		Item item = new Item();
		item = itemRepository.findById(itemId).get();
		Transaction _Transaction = new Transaction();
		_Transaction.setPaid(_transaction.getPaid());
		_Transaction.setItem(item);
		_Transaction.setDate(_transaction.getDate());
		transactionRepository.save(_Transaction);
		return _Transaction;
	}

	public List<Transaction> GetAll() {
		return transactionRepository.findAll();
	}

}
