package com.microservice.Notifications.Controller;



import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.microservice.Notifications.Dto.Message;

@RestController
public class RestApiController {
/*	@Autowired
	private Source source;
	
	@GetMapping(value = "/api/publish")
	public void sendMessage(){
		Message message = new Message();
		
		message.setMessage("Exceeded the maximum exchange limit !");
		source.output().send(MessageBuilder.withPayload(message).build());


	
	}
	
	*/
	private static final Logger LOG = Logger.getLogger(RestApiController.class.getName());
	
	@StreamListener(target =  Sink.INPUT)
	public void handleMessage(Message message){
		LOG.info("Received............");
		System.out.println("Received Message is: " + message.getMessage());
	}
	
}
