package com.microservice.spending.Dto;

import java.sql.Date;

public class ReadTransaction {
	private float paid;
	private Integer id;
	private Date date;
	
	public float getPaid() {
		return paid;
	}
	public void setPaid(float paid) {
		this.paid = paid;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	

}
