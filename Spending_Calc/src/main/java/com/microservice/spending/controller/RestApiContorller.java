package com.microservice.spending.controller;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.stream.messaging.Source;
import com.microservice.spending.Dto.Message;
import com.microservice.spending.Dto.ReadTransaction;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachePut;

@RestController
@EnableHystrix
@RequestMapping("/spending")
public class RestApiContorller {

	@Autowired
	private Source source;

	@Value("${eureka.instance.metadataMap.zone}")
	private String zone;
	@Autowired
	CacheManager cacheManager;
	@Autowired
	private RestTemplate restTemplate;

	private static final Logger LOG = Logger.getLogger(RestApiContorller.class.getName());

	@RequestMapping(value = "/zone", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public ResponseEntity<String> GetZone() {
		String _zone = "I'm in zone " + zone;
		return new ResponseEntity<String>(_zone, HttpStatus.OK);
	}

	public static final String KEY = "cacheKey";

	@RequestMapping(value = "/get", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	@HystrixCommand(fallbackMethod = "fallback_hello", commandProperties = {
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000"),
			@HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "5"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "30"),
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000"),
			@HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "1000") })
	@CachePut(value = "sumcache", key = "#root.target.KEY")
	public ResponseEntity<Float> GetSum() {
		float sum = 0;
		String url = "http://Transaction-service/transaction/get";
		LOG.info("before............");
		ResponseEntity<ReadTransaction[]> res = restTemplate.getForEntity(url, ReadTransaction[].class);
		List<ReadTransaction> object = Arrays.asList(res.getBody());
		for (ReadTransaction readTransaction : object) {
			sum += readTransaction.getPaid();
		}

		LOG.info("after............");
		if (sum >= 5000) {
			Message message = new Message();

			message.setMessage("Exceeded the maximum exchange limit !");
			source.output().send(MessageBuilder.withPayload(message).build());

		}

		return new ResponseEntity<Float>(sum, HttpStatus.OK);

	}

	public ResponseEntity<Float> fallback_hello() {
//		return "Request fails. It takes long time to response";
		float sum1 = -1;
		ValueWrapper w = cacheManager.getCache("sumcache").get("cacheKey");
		if (w != null) {
			return (ResponseEntity<Float>) w.get();
		} else {
			return new ResponseEntity<Float>(sum1, HttpStatus.OK);
		}

	}
}
