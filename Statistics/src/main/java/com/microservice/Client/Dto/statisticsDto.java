package com.microservice.Client.Dto;

public class statisticsDto {
	
	private float Sum;
	private Long itemCount;
	
	public float getSum() {
		return Sum;
	}
	public void setSum(float sum) {
		Sum = sum;
	}
	public Long getItemCount() {
		return itemCount;
	}
	public void setItemCount(Long itemCount) {
		this.itemCount = itemCount;
	}

}
