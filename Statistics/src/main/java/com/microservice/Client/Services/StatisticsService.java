package com.microservice.Client.Services;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class StatisticsService {
	
	
	@Autowired
	private RestTemplate restTemplate;
	

//	private static final Logger LOG = Logger.getLogger(RestApiContorller.class.getName());
	
	 @Async
	 public CompletableFuture<Float> LookSpending(String url){
	//	RestTemplate restTemplate = getre();
		 Float sp = restTemplate.getForObject(url, Float.class);
		 
		/* WebClient webClient = WebClient.create("http://localhost:8090");
		 Mono<Float> f =webClient.get()
	        .uri("/spending/get")
	        .retrieve()
	        .bodyToMono(Float.class);*/
		 
		 return CompletableFuture.completedFuture(sp);	 
	 }
	 
	// @LoadBalanced
	 public RestTemplate getre() {
		 return new RestTemplate();
		 
	 }
	 
	 @Async
	 public CompletableFuture<Long> LookitemCount(String url){
		// RestTemplate restTemplate= RestTemplateComponentFix.getRestTemplate();
		 
		 Long itemcount = restTemplate.getForObject(url, Long.class);
		 return CompletableFuture.completedFuture(itemcount);	 
	 }

}
