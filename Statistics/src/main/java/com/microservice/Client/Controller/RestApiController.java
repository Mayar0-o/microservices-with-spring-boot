package com.microservice.Client.Controller;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.microservice.Client.Dto.statisticsDto;
import com.microservice.Client.Services.StatisticsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping("/statistics")
public class RestApiController {

	@Autowired
	StatisticsService Statisticsservice;
	
	

	@RequestMapping(value = "/get", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public ResponseEntity<statisticsDto> GetStatistics() throws Throwable {
		statisticsDto s = new statisticsDto();
		String SpendingUrl = "http://localhost:8090/spending/get";
		String itemUrl = "http://localhost:8013/item/getcount";
		CompletableFuture<Float> sum = Statisticsservice.LookSpending(SpendingUrl);
		CompletableFuture<Long> it = Statisticsservice.LookitemCount(itemUrl);

		CompletableFuture.allOf(it,sum).join();

		try {
			s.setItemCount(it.get());
			s.setSum(sum.get());

		} catch (Throwable e) {
			throw e.getCause();
		}

		return new ResponseEntity<statisticsDto>(s, HttpStatus.OK);
	}

}
