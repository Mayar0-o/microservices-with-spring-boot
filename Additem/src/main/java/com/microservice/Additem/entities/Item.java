package com.microservice.Additem.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.GenerationType;


@Entity
@Table(name = "Item")
public class Item {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;	
	@NotNull(message = "Name Cannot be Empty")
	@NotBlank
	private String name;
	
	/*@ManyToOne
	@JoinColumn(insertable = true, name = "UserId", nullable = false)
	private User user;
	
	@JsonIgnore
	public User getUserId() {
		return user;
	}
	
	public void setUserId(User userId) {
		this.user = userId;
	}*/
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}


}
