package com.microservice.Additem.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.microservice.Additem.entities.Item;
import com.microservice.Additem.entities.User;
	
public interface ItemRepository extends JpaRepository<Item, Integer> {

	
}
