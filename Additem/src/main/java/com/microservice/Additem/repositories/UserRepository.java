package com.microservice.Additem.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.microservice.Additem.entities.User;



public interface UserRepository extends JpaRepository<User, Integer> {

}
