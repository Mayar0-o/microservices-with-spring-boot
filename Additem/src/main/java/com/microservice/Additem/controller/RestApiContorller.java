package com.microservice.Additem.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.microservice.Additem.entities.Item;
import com.microservice.Additem.entities.User;
import com.microservice.Additem.repositories.ItemRepository;
import com.microservice.Additem.repositories.UserRepository;

@RestController
@RequestMapping("/item")
public class RestApiContorller {

	@Autowired
	private ItemRepository itemRepository;
	/*@Autowired
	private UserRepository userRepository;
*/
	@PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Item> AddItem(@Valid @RequestBody Item item) {
		Item _item = new Item();
		_item.setName(item.getName());
		itemRepository.save(item);
		return new ResponseEntity<Item>(_item, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/get", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public List<Item> GetItem(/*@PathVariable("id") Integer id*/) {	
		//Optional<User> user = userRepository.findById(id);
		//List<Item> itemlist = user.get().getItem();		
		List<Item> itemlist =itemRepository.findAll();
		return itemlist;
	}
	@RequestMapping(value = "/getcount", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public ResponseEntity<Long> getcount(/*@PathVariable("id") Integer id*/) {	
		long count =itemRepository.count();
		return new ResponseEntity<Long>(count, HttpStatus.OK);
	}
	

}
